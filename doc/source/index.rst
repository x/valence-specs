.. valence-specs documentation master file

======================
Valence Specifications
======================

Ocata specs:

.. toctree::
   :glob:
   :maxdepth: 1

   specs/ocata/approved/*
   specs/pike/approved/*

==================
Indices and tables
==================

* :ref:`search`
