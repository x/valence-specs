..

 This work is licensed under a Creative Commons Attribution 3.0 Unported
 License.

http://creativecommons.org/licenses/by/3.0/legalcode

====================================
Refactor to support Multi-PodManager
====================================


This proposal of refactoring the codes to support multi-PodManager in valence
platform fully.

BP link : https://blueprints.launchpad.net/openstack-valence/+spec/refactor-to-support-multi-podmanager


Problem description
===================

As multi pod manager logic service seems ready in valence , but the valence api
and logic were still focus on the locked on pod manager base on the options in
the config file. So this would be removed and changed.

Also now our controller api and redfish.py are take effect on the locked pod manager.
This also need to abstract them out to support multi-podmanager.

So we would take a refactor on the code to make this change.


Proposed change
===============

Remove the options of pod manager in the config file. And get the pod manager
from the database if needed.

Bring up the relations between pod manager and components if need , like nodes
and systems in database. Should add a column 'podm_uuid' in node's db schema.

Split some pod manager api logic from 'valence.redfish.redfish.py' to a new feature
like 'pod_manager_api.py' or 'pod_manager_connection.py' that provide all the
needed functions of pod manager by input or specify a pod manager connection properties
which coming from database (Seems like the db.api.db_connection that we input
connection properties then call the db functions).Like this:
https://review.openstack.org/#/c/445360/3/valence/redfish/pod_manager_api.py

So that, valence.redfish.redfish.py could do resources operations cross multi-podmanager.
Like, get systems/nodes list from all multi-podmanager by its pod_manager_connection.

Then we could customize a connection pool (local cache) for pod managers to make
a better performance.

Also, we could abstract out a 'http_client.py' to provide all the http request
methods for pod manager client to implement its functions. Like this:
https://review.openstack.org/#/c/445360/2/valence/common/http_adapter.py

So this whole logic could be changed like this:
Controller transfer request to redfish.py.
Redfsh.py get connection from local cache (connection pool).
Then the connection provide the functions of the pod manager.
The functions would call 'http_client.py' to real pod manager service.
And sometimes, redfish.py could call http_client.py directly if the url is clearly
like get node detail info.

Alternatives
------------
Integrate all the pods into one pod manager.Let users do the integration.
Valence just lock the one pod manager in the config file.

Data model impact
-----------------
Insert a column 'podm_uuid' into model nodes' db schema.

REST API impact
---------------
This refactor would do a little change to exist apis , such as:

```
/v1/nodes/:
POST : add a new param to let user specify a exist pod manager to compose a new node.
GET : add a new param to do filter by specify a pod manager.
```

```
/v1/podmanagers/:
GET : add some usage info in returns to help user to do the selection.
```

```
/v1/podmanager/{uuid}:
DELETE : add some check logic to make save-deletion operation. Like we should
ensure all the nodes which created by valence are released when we do the deletion.
```

```
/v1/systems/:
GET : returns all pod managers' systems not only the systems of locked one podm now.
GET : add a new param to do filter by specify a pod manager.
```

Driver API impact
-----------------
None

Nova driver impact
------------------
None

Security impact
---------------
None

Other end user impact
---------------------
Some api-ref would be changed (TODO)

Scalability impact
------------------
Much improve valence's scalability

Performance Impact
------------------
None

Other deployer impact
---------------------
This would change the way how pod manager registered into valence. We would
register pod managers in frontend or CLI after Valence service started, instead
of configuring them in config file.

Developer impact
----------------
This will change a lot, because pod manager data is the parent data for exist
components from data structure level.Chassis, rack, composed node are under
a pod manager. And some component should have a column to show its belonging
pod manager.

Valence GUI / Horizon impact
----------------------------
Pod managers management site may need to be added and improved, as well as the
compose node apply forms should add a drop-down-menu to specify a pod manager.


Implementation
==============
Assignee(s)
-----------
Primary assignee:
  Bian.Hu

Other contributors:
  Mao Haijun


Work Items
----------
* update DB schema for node to add the releation of pod manager
* create pod_manager_api.py and implement function needed
* refactor refish.py abstract out needed function into pod_manager_api.py
* refactor logic of composing new node as well as its API's args definition
* implement http_client.py and its base functions
* implement pod manager scheduler
* implement pod manager safe deletion


Dependencies
============
None

Testing
=======
* Unit tests

Documentation Impact
====================
None

References
==========
None
